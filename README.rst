GSE software for the ASPA instrument.


Installation

ZWO sdk (17/2/2022 ArchLinux)
download SDK (ASI_linux_mac_SDK_V1.21.tar.bz2) from https://astronomy-imaging-camera.com/software-drivers
extract it
Deconnect Camera
> cd ASI_linux_mac_SDK_V1.21
> sudo cp -a lib/x64/libASICamera2* /usr/local/lib
> sudo chown root:root /usr/local/lib/libASICamera2.*
> sudo ldconfig
> sudo cp include/ASICamera2.h /usr/local/include/
> cd lib
> sudo install asi.rules /lib/udev/rules.d
Reconnect Camera
run 'cat /sys/module/usbcore/parameters/usbfs_memory_mb' to make sure the result is 200

