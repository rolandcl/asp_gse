from ctypes import *
from ctypes.util import find_library
import traceback

from asp_gse.config import *

CMODS6 = b"CmodS6"

# Load C library
libdepp = cdll.LoadLibrary(find_library("depp"))
libdmgr = cdll.LoadLibrary(find_library("dmgr"))

class CmodS6Controller(object):
    def __init__(self):
        super().__init__()
        self.__hif = c_uint32()
        self.__sz_dvc = c_char_p(CMODS6)
        # Init interface to CMOD S6 Board
        self.init_ok = False
        if libdmgr.DmgrOpen(byref(self.__hif), self.__sz_dvc):
            if libdepp.DeppEnable(self.__hif):
                self.init_ok = True
                self.reset_adf()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        libdepp.DeppPutReg(self.__hif, 0x20, 2, False)
        libdepp.DeppPutReg(self.__hif, 0x50, 2, False)
        print("CmodS6Controller.__exit__")
        print("!  ", exc_type)
        print("!  ", exc_val)
        if exc_tb is not None:
            print("!  ", traceback.print_tb(exc_tb))
        self.close()
        return True

    def get_temperature(self, i):
        spi_status = c_ubyte()
        result_msb = c_ubyte()
        result_lsb = c_ubyte()
        libdepp.DeppPutReg(self.__hif, 0x60, i, False)
        while True:
            libdepp.DeppGetReg(self.__hif, 0x62, byref(spi_status), False)
            if spi_status.value == 0:
                break
            time.sleep(0.01)
        libdepp.DeppGetReg(self.__hif, 0x60, byref(result_msb), False)
        libdepp.DeppGetReg(self.__hif, 0x61, byref(result_lsb), False)
        t = (result_msb.value * 256 + result_lsb.value) / 16
        return t

    def get_temperatures(self):
        return [self.get_temperature(i) for i in range(4)]

    def reset_adf(self):
        self.send_command(0, 3, DEFAULT_FUNC_REG)
        self.send_command(0, 2, DEFAULT_FUNC_REG)
        self.send_command(0, 1, DEFAULT_REF_REG)
        self.send_command(0, 6)
        self.send_command(1, 3, DEFAULT_FUNC_REG)
        self.send_command(1, 2, DEFAULT_FUNC_REG)
        self.send_command(1, 1, DEFAULT_REF_REG)
        self.send_command(1, 6)

    def set_aotf_freq1(self, freq):
        if freq == 0:
            libdepp.DeppPutReg(self.__hif, 0x20, 2, False)
        else:
            r = DEFAULT_REF_REG >> 2
            step = REF_FREQ / r / 8
            if freq < 125000:
                step /= 2
            ba = int(1000*freq/step + 0.5)
            self.send_command(0, 4, ba << 2)
            self.send_command(0, 6)
            if freq < 125000:
                libdepp.DeppPutReg(self.__hif, 0x20, 1, False)
            else:
                libdepp.DeppPutReg(self.__hif, 0x20, 0, False)

    def set_aotf_freq2(self, freq):
        if freq == 0:
            libdepp.DeppPutReg(self.__hif, 0x50, 2, False)
        else:
            r = DEFAULT_REF_REG >> 2
            step = REF_FREQ / r / 8
            if freq < 125000:
                step /= 2
            ba = int(1000*freq/step + 0.5)
            self.send_command(1, 4, ba << 2)
            self.send_command(1, 6)
            if freq < 125000:
                libdepp.DeppPutReg(self.__hif, 0x50, 1, False)
            else:
                libdepp.DeppPutReg(self.__hif, 0x50, 0, False)

    def send_command(self, chain, cmd, *args):
        if chain == 0:
            B = 0
        else:
            B = 0x30
        if cmd == 1:
            libdepp.DeppPutReg(self.__hif, B+0x00, args[0] >> 16, False)
            libdepp.DeppPutReg(self.__hif, B+0x01, (args[0] >> 8) & 0xff, False)
            libdepp.DeppPutReg(self.__hif, B+0x02, args[0] & 0xff, False)
        elif cmd == 2:
            libdepp.DeppPutReg(self.__hif, B+0x08, args[0] >> 16, False)
            libdepp.DeppPutReg(self.__hif, B+0x09, (args[0] >> 8) & 0xff, False)
            libdepp.DeppPutReg(self.__hif, B+0x0a, args[0] & 0xff, False)
        elif cmd == 3:
            libdepp.DeppPutReg(self.__hif, B+0x0c, args[0] >> 16, False)
            libdepp.DeppPutReg(self.__hif, B+0x0d, (args[0] >> 8) & 0xff, False)
            libdepp.DeppPutReg(self.__hif, B+0x0e, args[0] & 0xff, False)
        elif cmd == 4:
            libdepp.DeppPutReg(self.__hif, B+0x04, args[0] >> 16, False)
            libdepp.DeppPutReg(self.__hif, B+0x05, (args[0] >> 8) & 0xff, False)
            libdepp.DeppPutReg(self.__hif, B+0x06, args[0] & 0xff, False)
        elif cmd == 6:
            libdepp.DeppPutReg(self.__hif, B+0x0f, 0, False)
            libdepp.DeppPutReg(self.__hif, B+0x0b, 0, False)
            libdepp.DeppPutReg(self.__hif, B+0x03, 0, False)
            libdepp.DeppPutReg(self.__hif, B+0x07, 0, False)
        elif cmd == 7:
            libdepp.DeppPutReg(self.__hif, B+0x24, 0, False)
        elif cmd == 8:
            libdepp.DeppPutReg(self.__hif, B+0x20, 0, False)
            libdepp.DeppPutReg(self.__hif, B+0x21, 1, False)
            libdepp.DeppPutReg(self.__hif, B+0x22, 0, False)
            libdepp.DeppPutReg(self.__hif, B+0x23, 2, False)
            libdepp.DeppPutReg(self.__hif, B+0x24, 1, False)
        elif cmd == 10:
            ack = c_ubyte()
            libdepp.DeppGetReg(self.__hif, B+0x10, byref(ack), False)
            addrs = ByteArray16(* range(B, B+16))
            regs = ByteArray16()
            libdepp.DeppGetRegSet(self.__hif, addrs, regs, 16, False)
            buf = array.array('B', regs).tobytes()
            self.ack_sig.emit(ack.value, buf)

    def close(self):
        libdepp.DeppDisable(self.__hif);
        libdmgr.DmgrClose(self.__hif)

