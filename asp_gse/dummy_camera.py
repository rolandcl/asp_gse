#from contextlib import ExitStack
from datetime import datetime
import queue
import threading
import time

import numpy as np
from PIL import Image
from PyQt5.QtCore import *

from asp_gse.asp_frame import AspFrame
from asp_gse.config import *

class CameraThread(QObject, threading.Thread):
    new_img_sig1 = pyqtSignal(AspFrame)
    new_img_sig2 = pyqtSignal(AspFrame)

    def __init__(self, queue):
        super().__init__()
        self.__running = True
        self.__live_mode = False
        self.__reinit_pending = False
        self.__w_left1   = 0
        self.__w_top1    = 0
        self.__w_right1  = 1919
        self.__w_bottom1 = 1079
        self.__w_left2   = 0
        self.__w_top2    = 0
        self.__w_right2  = 1919
        self.__w_bottom2 = 1079
        self.__binx = 1
        self.__biny = 1
        self.__integ_time1 = DEFAULT_INTEG_TIME
        self.__integ_time2 = DEFAULT_INTEG_TIME
        self.__aotf_freq1 = DEFAULT_AOTF_FREQ
        self.__aotf_freq2 = DEFAULT_AOTF_FREQ
        self.__frames_per_trigger = 1
        self.__queue = queue

    def stop(self):
        self.__running = False

    def trigger(self):
        self.__queue.put((int(self.__aotf_freq1), int(self.__aotf_freq2)))

    def live_mode(self, flag):
        self.__live_mode = flag

    def set_w_left1(self, w_left):
        self.__w_left1 = w_left
        self.__reinit_pending = True

    def set_w_right1(self, w_right):
        self.__w_right1 = w_right
        self.__reinit_pending = True

    def set_w_top1(self, w_top):
        self.__w_top1 = w_top
        self.__reinit_pending = True

    def set_w_bottom1(self, w_bottom):
        self.__w_bottom1 = w_bottom
        self.__reinit_pending = True

    def set_w_left2(self, w_left):
        self.__w_left2 = w_left
        self.__reinit_pending = True

    def set_w_right2(self, w_right):
        self.__w_right2 = w_right
        self.__reinit_pending = True

    def set_w_top2(self, w_top):
        self.__w_top2 = w_top
        self.__reinit_pending = True

    def set_w_bottom2(self, w_bottom):
        self.__w_bottom2 = w_bottom
        self.__reinit_pending = True

    def set_binx(self, binx):
        self.__binx = binx
        #self.__reinit_pending = True

    def set_biny(self, biny):
        self.__biny = biny
        #self.__reinit_pending = True

    def set_frames_per_trigger(self, n):
        self.__frames_per_trigger = n
        self.__reinit_pending = True

    def set_integ_time1(self, integ_time):
        self.__integ_time1 = integ_time

    def set_integ_time2(self, integ_time):
        self.__integ_time2 = integ_time

    def set_aotf_freq1(self, aotf_freq):
        self.__aotf_freq1 = aotf_freq

    def set_aotf_freq2(self, aotf_freq):
        self.__aotf_freq2 = aotf_freq


    def run(self):
        test_pattern = np.asarray(Image.open("test_pattern.png"), np.uint16)
        print("test_pattern", test_pattern.dtype, test_pattern.shape)
        curr_freq1 = None
        curr_freq2 = None
        last_trigger_time = 0
        while self.__running:
            self.__reinit_pending = False
            curr_integ_time = None
            cam1_frames_per_trigger_zero_for_unlimited = self.__frames_per_trigger
            cam2_frames_per_trigger_zero_for_unlimited = self.__frames_per_trigger
            cam1_image_poll_timeout_ms = 10  # 0.01 second polling timeout
            cam2_image_poll_timeout_ms = 10  # 0.01 second polling timeout
            print("Reinit cameras")
            #print(self.camera_info(camera))
            cam1_roi = (self.__w_left1, self.__w_top1,
                          self.__w_right1, self.__w_bottom1)
            cam2_roi = (self.__w_left2, self.__w_top2,
                          self.__w_right2, self.__w_bottom2)
            cam1_binx = 1
            cam1_biny = 1
            cam2_binx = 1
            cam2_biny = 1
            cam1_exposure_time_us = self.__integ_time1
            cam2_exposure_time_us = self.__integ_time2
            frames_per_trigger = cam1_frames_per_trigger_zero_for_unlimited or 1
            frame_count1 = 0
            frame_count2 = 0
            while self.__running and not self.__reinit_pending:
                try:
                    if self.__live_mode:
                        freq1 = self.__aotf_freq1
                        freq2 = self.__aotf_freq2
                        time.sleep(0.5)
                    else:
                        freq1, freq2 = self.__queue.get(timeout=0.1)
                except queue.Empty:
                    continue
                else:
                    if freq1 is None:
                        continue
                    sleep_flag = False
                    if freq1 != curr_freq1:
                        time.sleep(0.2) # cmods6.set_aotf_freq1(freq1)
                        sleep_flag = True
                    if freq2 != curr_freq2:
                        time.sleep(0.2) # cmods6.set_aotf_freq2(freq2)
                        sleep_flag = True
                    if sleep_flag:
                        time.sleep(0.02)

                    cam1_exposure_time_us = self.__integ_time1
                    cam2_exposure_time_us = self.__integ_time2
                    binx = self.__binx
                    biny = self.__biny

                    now = time.time()
                    sleep_time = 0.3 - now + last_trigger_time
                    if sleep_time > 0:
                        time.sleep(sleep_time)

                    last_trigger_time = time.time()
                    temp0 = 0
                    temp1 = 0
                    frame1 = None
                    frame2 = None
                    for _ in range(frames_per_trigger):
                        time.sleep(cam1_exposure_time_us/1e6)
                        while frame1 is None:
                            (x0, y0, x1, y1) = cam1_roi
                            frame1 = test_pattern[y0:y1+1, x0:x1+1]
                            frame1 = (frame1 * (cam1_exposure_time_us/1e6)).astype(np.uint16)
                            print("frame1", frame1.shape, frame1[:50,:10])
                        if cam2_exposure_time_us > cam1_exposure_time_us:
                            dt = cam2_exposure_time_us - cam1_exposure_time_us
                            time.sleep(dt/1e6)
                        while frame2 is None:
                            (x0, y0, x1, y1) = cam2_roi
                            frame2 = test_pattern[y0:y1+1, x0:x1+1]
                            frame2 = (frame2 * (cam2_exposure_time_us/1e6)).astype(np.uint16)
                        dtime = datetime.now()

                        frame_count1 += 1
                        fr1 = np.copy(frame1)
                        frame_count2 += 1
                        fr2 = np.copy(frame2)
                        print("Cam1 frame #{} received!".format(frame_count1))
                        print("Cam2 frame #{} received!".format(frame_count2))
                        time.sleep(0.01)

                        asp_fr = AspFrame(fr1, cam1_roi[0], cam1_roi[1],
                                              binx, biny)
                        asp_fr.dtime = dtime
                        asp_fr.aotf_freq = freq1
                        asp_fr.frame_cnt = frame_count1
                        asp_fr.integ_time = cam1_exposure_time_us
                        asp_fr.temp0 = temp0
                        asp_fr.temp1 = temp1
                        asp_fr.camera = CAMERA_1
                        self.new_img_sig1.emit(asp_fr)

                        asp_fr = AspFrame(fr2, cam2_roi[0], cam2_roi[1],
                                              binx, biny)
                        asp_fr.dtime = dtime
                        asp_fr.aotf_freq = freq2
                        asp_fr.frame_cnt = frame_count2
                        asp_fr.integ_time = cam2_exposure_time_us
                        asp_fr.temp0 = temp0
                        asp_fr.temp1 = temp1
                        asp_fr.camera = CAMERA_2
                        self.new_img_sig2.emit(asp_fr)



    def camera_info(self, camera):
        buf = ""
        buf += "model = %s\n" % camera.model
        buf += "usb_port_type = %s\n" % camera.usb_port_type
        buf += "get_measured_frame_rate_fps = %s\n" % camera.get_measured_frame_rate_fps()
        buf += "sensor_readout_time_ns = %s\n" % str(camera.sensor_readout_time_ns)
        buf += "exposure_time_range_us = %s\n" % str(camera.exposure_time_range_us)
        buf += "is_eep_supported = %s\n" % camera.is_eep_supported
        buf += "is_hot_pixel_correction_enabled = %s\n" % camera.is_hot_pixel_correction_enabled
        buf += "binx_range = %s\n" % str(camera.binx_range)
        buf += "biny_range = %s\n" % str(camera.biny_range)
        buf += "roi_range = %s\n" % str(camera.roi_range)
        buf += "sensor_width_pixels = %s\n" % camera.sensor_width_pixels
        buf += "sensor_height_pixels = %s\n" % camera.sensor_height_pixels
        buf += "data_rate = %s\n" % camera.data_rate
        buf += "bit_depth = %s\n" % camera.bit_depth
        buf += "gain_range = %s\n" % str(camera.gain_range)
        buf += "frames_per_trigger_range = %s\n" % str(camera.frames_per_trigger_range)
        return buf

