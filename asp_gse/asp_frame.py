import numpy as np
from PyQt5.QtGui import *

class AspFrame( object ):
    def __init__(self, tl_frame, x, y, binx, biny):
        self.frame = tl_frame
        self.x     = x
        self.y     = y
        self.binx  = binx
        self.biny  = biny

    def to_qimage(self, gamma, log_flag):
        fr = self.frame
        if log_flag:
            fr = np.log2(fr+1) * (255/16)
        else:
            if gamma != 1.0:
                fr = np.power(fr / 65536.0, gamma) * 256
            else:
                fr = np.right_shift(fr, 8)
        fr = fr.astype('uint8')
        qimg = QImage(fr, self.frame.shape[1], self.frame.shape[0], QImage.Format_Grayscale8)
        return qimg

