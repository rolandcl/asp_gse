from concurrent.futures import ThreadPoolExecutor
import os.path
import queue
import sys
import time

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.figure import Figure
import matplotlib.ticker as ticker
import numpy as np

# from camera_controller import CameraThread
from asp_gse.zwo_camera_controller import CameraThread
# from asp_gse.dummy_camera import CameraThread
from asp_gse.scenario_controller import ScenarioThread
from asp_gse.tar_controller import TarFile
from asp_gse.config import *

# class ImageWidget(QWidget):
#     def __init__( self ):
#         QWidget.__init__( self )
#         self.__asp_frame = None
#         self.__sel_row = None
#         self.resize(420, 290)

#     def show_image(self, asp_frame, sel_row, gamma, log_flag):
#         self.setAutoFillBackground(True)
#         palette = QPalette(QColor(WIDGET_BKG_COLOR))
#         self.setPalette(palette)
#         self.__asp_frame = asp_frame
#         self.__sel_row = sel_row
#         self.__gamma = gamma
#         self.__log_flag = log_flag
#         self.update()

#     def paintEvent(self, ev):
#         if self.__asp_frame:
#             fr = self.__asp_frame
#             painter = QPainter(self)
#             transform = QTransform()
#             transform = transform.scale(0.10, 0.10)
#             transform = transform.translate(fr.x, fr.y)
#             painter.setTransform(transform)
#             painter.drawImage(0, 0, fr.to_qimage(self.__gamma, self.__log_flag))
#             painter.resetTransform()
#             y = int(self.__sel_row * 0.1 + 0.5)
#             painter.setPen(Qt.magenta)
#             painter.drawLine(0, y, 419, y)


def apply_gamma(img, dxy, gamma, log_flag):
    if log_flag:
        img = np.log2(img+1) * (255/16)
    else:
        if gamma != 1.0:
            img = np.power(img / 65536.0, gamma) * 256
        else:
            img = np.right_shift(img, 8)
    img = img.astype('uint8')
    return img, dxy


class ImageWidget(QWidget):
    WIDTH = 420
    HEIGHT = 290

    def __init__(self):
        QWidget.__init__(self)
        self.__img = None
        self.__sel_row = None
        self.resize(self.WIDTH, self.HEIGHT)

    def show_image(self, img, sel_row, dxy=(0, 0)):
        self.setAutoFillBackground(True)
        palette = QPalette(QColor(WIDGET_BKG_COLOR))
        self.setPalette(palette)
        self.__img = img
        self.__sel_row = sel_row
        self.__dxy = dxy
        self.update()

    def paintEvent(self, ev):
        if self.__img is not None:
            img = self.__img
            painter = QPainter(self)
            transform = QTransform()
            transform = transform.scale(0.10, 0.10)
            transform = transform.translate(*self.__dxy)
            painter.setTransform(transform)
            h, w = img.shape
            qimg = QImage(img, w, h, QImage.Format_Grayscale8)
            painter.drawImage(0, 0, qimg)
            painter.resetTransform()
            if self.__sel_row is not None:
                y = int(self.__sel_row * 0.1 + 0.5)
                painter.setPen(Qt.magenta)
                painter.drawLine(0, y, self.WIDTH-1, y)


class CameraWindow(QScrollArea):
    def __init__(self):
        super().__init__()
        # self.__scroll_area = QScrollArea()
        self.image_w = ImageWidget()
        self.setWidget(self.image_w)


class CameraControlsDock(QDockWidget):
    def __init__(self, title):
        super().__init__(title)

        self.frame = QFrame()
        layout = QGridLayout()
        self.frame.setLayout(layout)
        self.setWidget(self.frame)

        layout.addWidget(QLabel("IntegTime"), 0, 0)
        self.integ_time_sb = QSpinBox()
        self.integ_time_sb.setRange(29, MAX_INTEG_TIME)
        self.integ_time_sb.setValue(DEFAULT_INTEG_TIME)
        self.integ_time_sb.setSuffix(" µs")
        layout.addWidget(self.integ_time_sb, 0, 1)

        layout.addWidget(QLabel("BinX"), 1, 0)
        self.binx_sb = QSpinBox()
        self.binx_sb.setRange(1, 16)
        self.binx_sb.setValue(1)
        layout.addWidget(self.binx_sb, 1, 1)

        layout.addWidget(QLabel("BinY"), 2, 0)
        self.biny_sb = QSpinBox()
        self.biny_sb.setRange(1, 16)
        self.biny_sb.setValue(1)
        layout.addWidget(self.biny_sb, 2, 1)

        layout.addWidget(QLabel("WinLeft"), 3, 0)
        self.w_left = QSpinBox()
        self.w_left.setRange(0, 4143)
        self.w_left.setValue(DEFAULT_W_LEFT)
        layout.addWidget(self.w_left, 3, 1)

        layout.addWidget(QLabel("WinRight"), 4, 0)
        self.w_right = QSpinBox()
        self.w_right.setRange(7, 4143)
        self.w_right.setValue(DEFAULT_W_RIGHT)
        layout.addWidget(self.w_right, 4, 1)

        layout.addWidget(QLabel("WinTop"), 5, 0)
        self.w_top = QSpinBox()
        self.w_top.setRange(0, 2820)
        self.w_top.setValue(DEFAULT_W_TOP)
        layout.addWidget(self.w_top, 5, 1)

        layout.addWidget(QLabel("WinBottom"), 6, 0)
        self.w_bottom = QSpinBox()
        self.w_bottom.setRange(1, 2821)
        self.w_bottom.setValue(DEFAULT_W_BOTTOM)
        layout.addWidget(self.w_bottom, 6, 1)

        layout.addWidget(QLabel("FramesPerTrigger"), 7, 0)
        self.frames_per_trigger_sb = QSpinBox()
        self.frames_per_trigger_sb.setRange(1, 16)
        self.frames_per_trigger_sb.setValue(1)
        layout.addWidget(self.frames_per_trigger_sb, 7, 1)

        layout.addWidget(QLabel("AOTFFreq"), 8, 0)
        self.aotf_freq_sb = QDoubleSpinBox()
        self.aotf_freq_sb.setRange(MIN_AOTF_FREQ, MAX_AOTF_FREQ)
        self.aotf_freq_sb.setDecimals(0)
        self.aotf_freq_sb.setSingleStep(100)
        self.aotf_freq_sb.setSuffix(" kHz")
        self.aotf_freq_sb.setValue(DEFAULT_AOTF_FREQ)
        layout.addWidget(self.aotf_freq_sb, 8, 1)

        self.acq_button = QPushButton("Trigger")
        layout.addWidget(self.acq_button, 9, 0)
        self.live_mode_button = QCheckBox("Live Mode")
        layout.addWidget(self.live_mode_button, 9, 1)
        self.live_mode_button.toggled.connect(self.acq_button.setDisabled)
        layout.setRowStretch(10, 1)


@ticker.FuncFormatter
def major_formatter(x, pos):
    return "%.0f" % (x*1e6)


def histo(img):
    p_max = img.max()
    return np.histogram(img.flat, bins=256, range=(0, p_max), density=True)


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.__scenario_path = None
        self.__scenario = None
        self.__queue = queue.Queue(1)

        self.__executor = ThreadPoolExecutor(max_workers=4)
        self.__histo_future1 = None
        self.__gamma_future1 = None
        self.__last_img_t1 = 0
        self.__last_gamma_t1 = 0
        self.__last_histo_t1 = 0
        self.__img_period1 = 0
        self.__gamma_period1 = 0
        self.__histo_period1 = 0
        self.__plot1_mutex = QMutex()

        self.__histo_future2 = None
        self.__gamma_future2 = None
        self.__last_img_t2 = 0
        self.__last_gamma_t2 = 0
        self.__last_histo_t2 = 0
        self.__img_period2 = 0
        self.__gamma_period2 = 0
        self.__histo_period2 = 0
        self.__plot2_mutex = QMutex()

        # Init GUI
        self.__mdi_area = QMdiArea()
        self.setCentralWidget(self.__mdi_area)

        container1 = QWidget()
        self.__mdi_sub_w1 = self.__mdi_area.addSubWindow(container1)
        self.__mdi_sub_w1.setWindowFlags(
            Qt.CustomizeWindowHint | Qt.WindowTitleHint)
        self.__mdi_sub_w1.setWindowTitle("Camera 1 (%s)" % CAMERA_1)
        layout1 = QVBoxLayout()
        container1.setLayout(layout1)
        self.__camera_win1 = CameraWindow()
        layout1.addWidget(self.__camera_win1)
        layout1h = QHBoxLayout()
        layout1.addLayout(layout1h)
        layout1h.addWidget(QLabel("Selected Row"))
        self.__sel_row1_sb = QSpinBox()
        self.__sel_row1_sb.setRange(0, 2821)
        self.__sel_row1_sb.setValue(DEFAULT_SELECTED_ROW1)
        layout1h.addWidget(self.__sel_row1_sb)
        layout1h.addSpacing(30)
        layout1h.addWidget(QLabel("Gamma"))
        self.__gamma1_sb = QDoubleSpinBox()
        self.__gamma1_sb.setRange(0.1, 5)
        self.__gamma1_sb.setValue(1.0)
        self.__gamma1_sb.setDecimals(2)
        self.__gamma1_sb.setSingleStep(0.05)
        layout1h.addWidget(self.__gamma1_sb)
        layout1h.addSpacing(30)
        self.__log1_cb = QCheckBox("Log")
        layout1h.addWidget(self.__log1_cb)
        layout1h.addStretch(1)
        fig1a = FigureCanvas(Figure(figsize=(ROW_PLOT_WIDTH, ROW_PLOT_HEIGHT)))
        self.__plot1_ax = fig1a.figure.subplots()
        self.__plot1_ax.set_title("Row ?")
        self.__plot1_ax.set_ylabel('intensity')
        layout1.addWidget(fig1a)
        fig1b = FigureCanvas(
            Figure(figsize=(HIST_PLOT_WIDTH, HIST_PLOT_HEIGHT)))
        self.__hist1_ax = fig1b.figure.subplots()
        self.__hist1_ax.set_title("Histogram")
        self.__hist1_ax.yaxis.set_major_formatter(major_formatter)
        layout1.addWidget(fig1b)

        container2 = QWidget()
        self.__mdi_sub_w2 = self.__mdi_area.addSubWindow(container2)
        self.__mdi_sub_w2.setWindowFlags(
            Qt.CustomizeWindowHint | Qt.WindowTitleHint)
        self.__mdi_sub_w2.setWindowTitle("Camera 2 (%s)" % CAMERA_2)
        layout2 = QVBoxLayout()
        container2.setLayout(layout2)
        self.__camera_win2 = CameraWindow()
        layout2.addWidget(self.__camera_win2)
        layout2h = QHBoxLayout()
        layout2.addLayout(layout2h)
        layout2h.addWidget(QLabel("Selected Row"))
        self.__sel_row2_sb = QSpinBox()
        self.__sel_row2_sb.setRange(0, 2821)
        self.__sel_row2_sb.setValue(DEFAULT_SELECTED_ROW2)
        layout2h.addWidget(self.__sel_row2_sb)
        layout2h.addSpacing(30)
        layout2h.addWidget(QLabel("Gamma"))
        self.__gamma2_sb = QDoubleSpinBox()
        self.__gamma2_sb.setRange(0.1, 5)
        self.__gamma2_sb.setValue(1.0)
        self.__gamma2_sb.setDecimals(2)
        self.__gamma2_sb.setSingleStep(0.05)
        layout2h.addWidget(self.__gamma2_sb)
        layout2h.addSpacing(30)
        self.__log2_cb = QCheckBox("Log")
        layout2h.addWidget(self.__log2_cb)
        layout2h.addStretch(1)
        fig2a = FigureCanvas(Figure(figsize=(ROW_PLOT_WIDTH, ROW_PLOT_HEIGHT)))
        self.__plot2_ax = fig2a.figure.subplots()
        self.__plot2_ax.set_title("Row ?")
        self.__plot2_ax.set_ylabel('intensity')
        layout2.addWidget(fig2a)
        fig2b = FigureCanvas(
            Figure(figsize=(HIST_PLOT_WIDTH, HIST_PLOT_HEIGHT)))
        self.__hist2_ax = fig2b.figure.subplots()
        self.__hist2_ax.set_title("Histogram")
        layout2.addWidget(fig2b)

        self.cam1_dock = CameraControlsDock("Camera 1 (%s)" % CAMERA_1)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.cam1_dock)
        self.cam2_dock = CameraControlsDock("Camera 2 (%s)" % CAMERA_2)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.cam2_dock)

        self.toolbar = self.addToolBar("Main Toolbar")
        self.scenario_but = QPushButton("Choose a Scenario")
        self.toolbar.addWidget(self.scenario_but)
        self.start_action = self.toolbar.addAction("Start")
        self.start_action.setDisabled(True)
        self.stop_action = self.toolbar.addAction("Stop")
        self.stop_action.setDisabled(True)

        self.camera_th = CameraThread(self.__queue)
        # self.camera_th.new_img_sig.connect(self.__camera_win1.image_w.show_image)
        #self.camera_th.new_img_sig1.connect(self.plot1)
        self.camera_th.new_img_sig2.connect(self.plot2)

        # Make Qt connections
        self.cam1_dock.acq_button.clicked.connect(self.camera_th.trigger)
        self.cam1_dock.live_mode_button.toggled.connect(
            self.camera_th.live_mode)
        self.cam1_dock.live_mode_button.toggled.connect(self.live_mode_changed)
        self.cam1_dock.binx_sb.valueChanged.connect(self.camera_th.set_binx)
        self.cam1_dock.biny_sb.valueChanged.connect(self.camera_th.set_biny)
        self.cam1_dock.w_left.valueChanged.connect(self.camera_th.set_w_left1)
        self.cam1_dock.w_right.valueChanged.connect(
            self.camera_th.set_w_right1)
        self.cam1_dock.w_top.valueChanged.connect(self.camera_th.set_w_top1)
        self.cam1_dock.w_bottom.valueChanged.connect(
            self.camera_th.set_w_bottom1)
        self.cam1_dock.integ_time_sb.valueChanged.connect(
            self.camera_th.set_integ_time1)
        self.cam1_dock.aotf_freq_sb.valueChanged.connect(
            self.camera_th.set_aotf_freq1)
        self.cam1_dock.frames_per_trigger_sb.valueChanged.connect(
            self.camera_th.set_frames_per_trigger)
        self.cam1_dock.frames_per_trigger_sb.valueChanged.connect(
            self.frames_per_trigger_changed)

        self.cam2_dock.acq_button.clicked.connect(self.camera_th.trigger)
        self.cam2_dock.live_mode_button.toggled.connect(
            self.camera_th.live_mode)
        self.cam2_dock.live_mode_button.toggled.connect(self.live_mode_changed)
        self.cam2_dock.binx_sb.valueChanged.connect(self.camera_th.set_binx)
        self.cam2_dock.biny_sb.valueChanged.connect(self.camera_th.set_biny)
        self.cam2_dock.w_left.valueChanged.connect(self.camera_th.set_w_left2)
        self.cam2_dock.w_right.valueChanged.connect(
            self.camera_th.set_w_right2)
        self.cam2_dock.w_top.valueChanged.connect(self.camera_th.set_w_top2)
        self.cam2_dock.w_bottom.valueChanged.connect(
            self.camera_th.set_w_bottom2)
        self.cam2_dock.integ_time_sb.valueChanged.connect(
            self.camera_th.set_integ_time2)
        self.cam2_dock.aotf_freq_sb.valueChanged.connect(
            self.camera_th.set_aotf_freq2)
        self.cam2_dock.frames_per_trigger_sb.valueChanged.connect(
            self.camera_th.set_frames_per_trigger)
        self.cam2_dock.frames_per_trigger_sb.valueChanged.connect(
            self.frames_per_trigger_changed)

        self.scenario_but.clicked.connect(self.choose_scenario)
        self.start_action.triggered.connect(self.start_scenario)
        self.stop_action.triggered.connect(self.stop_scenario)

        self.__refresh_gui_timer = QTimer()
        self.__refresh_gui_timer.timeout.connect(self._refresh_gui)
        self.__refresh_gui_timer.start(1000)

        self.camera_th.start()

    def _refresh_gui(self):
        self.plot1(None)
        self.plot2(None)

    def live_mode_changed(self, live_mode_en):
        if live_mode_en:
            self.cam1_dock.frames_per_trigger_sb.setValue(1)
            self.cam2_dock.frames_per_trigger_sb.setValue(1)

    def frames_per_trigger_changed(self, value):
        if value > 1:
            self.cam1_dock.live_mode_button.setChecked(False)
            self.cam2_dock.live_mode_button.setChecked(False)

    def plot1(self, asp_frame):
        if asp_frame is None and self.__plot1_mutex.tryLock():
            if self.__gamma_future1 is not None and self.__gamma_future1.done():
                img8b, dxy = self.__gamma_future1.result()
                self.__gamma_future1 = None
                sel_row = self.__sel_row1_sb.value()
                self.__camera_win1.image_w.show_image(img8b, sel_row, dxy)

            if self.__histo_future1 is not None and self.__histo_future1.done():
                hist, bin_edges = self.__histo_future1.result()
                self.__histo_future1 = None
                self.__hist1_ax.clear()
                self.__hist1_ax.set_title("Histogram")
                self.__hist1_ax.yaxis.set_major_formatter(major_formatter)
                self.__hist1_ax.set_ylabel('density (ppm)')
                self.__hist1_ax.plot(bin_edges[:-1], hist)
                self.__hist1_ax.figure.canvas.draw()
            self.__plot1_mutex.unlock()
            return

        self.__plot1_mutex.lock()
        t = time.time()
        img_period = t - self.__last_img_t1
        if img_period > 0.01:
            self.__last_img_t1 = t
        else:
            return

        gamma = self.__gamma1_sb.value()
        log_flag = self.__log1_cb.isChecked()
        sel_row = self.__sel_row1_sb.value()

        if self.__gamma_future1 is None:
            img = asp_frame.frame
            dxy = (asp_frame.x, asp_frame.y)
            self.__gamma_future1 = self.__executor.submit(
                apply_gamma, img, dxy, gamma, log_flag)
            self.__gamma_period1 = t - self.__last_gamma_t1
            self.__last_gamma_t1 = t
        elif self.__gamma_future1.done():
            img8b, dxy = self.__gamma_future1.result()
            self.__gamma_period1 = t - self.__last_gamma_t1
            if self.__gamma_period1 > 0.1:
                self.__last_gamma_t1 = t
                sel_row = self.__sel_row1_sb.value()
                self.__camera_win1.image_w.show_image(img8b, sel_row, dxy)

                img = asp_frame.frame
                dxy = (asp_frame.x, asp_frame.y)
                self.__gamma_future1 = self.__executor.submit(
                    apply_gamma, img, dxy, gamma, log_flag)

        if self.__histo_future1 is None:
            self.__histo_future1 = self.__executor.submit(histo, img)
            self.__histo_period1 = t - self.__last_histo_t1
            self.__last_histo_t1 = t
        elif self.__histo_future1.done():
            hist, bin_edges = self.__histo_future1.result()
            self.__histo_period1 = t - self.__last_histo_t1
            if self.__histo_period2 > 0.1:
                self.__last_histo_t1 = t

                self.__hist1_ax.clear()
                self.__hist1_ax.set_title("Histogram")
                self.__hist1_ax.yaxis.set_major_formatter(major_formatter)
                self.__hist1_ax.set_ylabel('density (ppm)')
                self.__hist1_ax.plot(bin_edges[:-1], hist)
                self.__hist1_ax.figure.canvas.draw()

                img = asp_frame.frame
                self.__histo_future1 = self.__executor.submit(histo, img)

        row_ix = sel_row - asp_frame.y
        self.__plot1_ax.clear()
        self.__plot1_ax.set_title("Row %d" % sel_row)
        self.__plot1_ax.set_ylabel('intensity')
        if row_ix >= 0 and row_ix < asp_frame.frame.shape[0]:
            row = asp_frame.frame[row_ix]
            if gamma != 1.0:
                row_max = row.max()
                if row_max > 0:
                    row = np.power(row / row_max, gamma) * row_max
            self.__plot1_ax.plot(row)
        self.__plot1_ax.figure.canvas.draw()

        print("1: %0.3f %0.3f %0.3f" %
              (self.__img_period1, self.__gamma_period1, self.__histo_period1))
        self.__plot1_mutex.unlock()

    def plot2(self, asp_frame):
        if asp_frame is None and self.__plot2_mutex.tryLock():
            if self.__gamma_future2 is not None and self.__gamma_future2.done():
                img8b, dxy = self.__gamma_future2.result()
                self.__gamma_future2 = None
                sel_row = self.__sel_row2_sb.value()
                self.__camera_win2.image_w.show_image(img8b, sel_row, dxy)

            if self.__histo_future2 is not None and self.__histo_future2.done():
                hist, bin_edges = self.__histo_future2.result()
                self.__histo_future2 = None
                self.__hist2_ax.clear()
                self.__hist2_ax.set_title("Histogram")
                self.__hist2_ax.yaxis.set_major_formatter(major_formatter)
                self.__hist2_ax.set_ylabel('density (ppm)')
                self.__hist2_ax.plot(bin_edges[:-1], hist)
                self.__hist2_ax.figure.canvas.draw()
            self.__plot2_mutex.unlock()
            return

        self.__plot2_mutex.lock()
        t = time.time()
        img_period = t - self.__last_img_t2
        if img_period > 0.01:
            self.__last_img_t2 = t
        else:
            return

        gamma = self.__gamma2_sb.value()
        log_flag = self.__log2_cb.isChecked()
        sel_row = self.__sel_row2_sb.value()

        if self.__gamma_future2 is None:
            img = asp_frame.frame
            dxy = (asp_frame.x, asp_frame.y)
            self.__gamma_future2 = self.__executor.submit(
                apply_gamma, img, dxy, gamma, log_flag)
            self.__gamma_period2 = t - self.__last_gamma_t2
            self.__last_gamma_t2 = t
        elif self.__gamma_future2.done():
            img8b, dxy = self.__gamma_future2.result()
            self.__gamma_period2 = t - self.__last_gamma_t2
            if self.__gamma_period2 > 0.1:
                self.__last_gamma_t2 = t
                sel_row = self.__sel_row2_sb.value()
                self.__camera_win2.image_w.show_image(img8b, sel_row, dxy)

                img = asp_frame.frame
                dxy = (asp_frame.x, asp_frame.y)
                self.__gamma_future2 = self.__executor.submit(
                    apply_gamma, img, dxy, gamma, log_flag)

        if self.__histo_future2 is None:
            self.__histo_future2 = self.__executor.submit(histo, img)
            self.__histo_period2 = t - self.__last_histo_t2
            self.__last_histo_t2 = t
        elif self.__histo_future2.done():
            hist, bin_edges = self.__histo_future2.result()
            self.__histo_period2 = t - self.__last_histo_t2
            if self.__histo_period2 > 0.1:
                self.__last_histo_t2 = t

                self.__hist2_ax.clear()
                self.__hist2_ax.set_title("Histogram")
                self.__hist2_ax.yaxis.set_major_formatter(major_formatter)
                self.__hist2_ax.set_ylabel('density (ppm)')
                self.__hist2_ax.plot(bin_edges[:-1], hist)
                self.__hist2_ax.figure.canvas.draw()

                img = asp_frame.frame
                self.__histo_future2 = self.__executor.submit(histo, img)

        row_ix = sel_row - asp_frame.y
        self.__plot2_ax.clear()
        self.__plot2_ax.set_title("Row %d" % sel_row)
        self.__plot2_ax.set_ylabel('intensity')
        if row_ix >= 0 and row_ix < asp_frame.frame.shape[0]:
            row = asp_frame.frame[row_ix]
            if gamma != 1.0:
                row_max = row.max()
                if row_max > 0:
                    row = np.power(row / row_max, gamma) * row_max
            self.__plot2_ax.plot(row)
        self.__plot2_ax.figure.canvas.draw()

        print("2: %0.3f %0.3f %0.3f" %
              (self.__img_period2, self.__gamma_period2, self.__histo_period2))
        self.__plot2_mutex.unlock()

    def choose_scenario(self):
        path, _ = QFileDialog.getOpenFileName(
            self, "Choose a scenario file", "scenarios")
        if path:
            self.__scenario_path = path
            self.scenario_but.setText("Scenario = %s" % os.path.basename(path))
            self.start_action.setEnabled(True)
        else:
            self.__scenario_path = None
            self.scenario_but.setText("Choose a Scenario")
            self.start_action.setDisabled(True)

    def start_scenario(self):
        if self.__scenario_path:
            self.__scenario = ScenarioThread(
                self.__scenario_path, self.__queue)
            self.stop_action.setEnabled(True)
            self.start_action.setDisabled(True)
            self.scenario_but.setDisabled(True)
            self.cam1_dock.live_mode_button.setChecked(False)
            self.cam2_dock.live_mode_button.setChecked(False)
            self.cam1_dock.live_mode_button.setDisabled(True)
            self.cam2_dock.live_mode_button.setDisabled(True)

            self.__scenario.finished.connect(self.scenario_finished)
            self.__scenario.change_camera_window.connect(
                self.change_camera_window)
            self.__scenario.change_binx.connect(self.change_camera_binx)
            self.__scenario.change_biny.connect(self.change_camera_biny)
            self.__scenario.change_integ_time.connect(
                self.change_camera_integ_time)
            self.__scenario.change_aotf_freq.connect(
                self.change_camera_aotf_freq)
            self.__scenario.change_frames_per_trigger.connect(
                self.change_camera_frames_per_trigger)
            self.__scenario.exec_line.connect(self.statusBar().showMessage)
            self.__tarfile = TarFile(self.__scenario_path)
            self.camera_th.new_img_sig1.connect(self.__tarfile.save_asp_frame)
            self.camera_th.new_img_sig2.connect(self.__tarfile.save_asp_frame)
            time.sleep(1)
            self.__scenario.start()

    def stop_scenario(self):
        if self.__scenario:
            self.__scenario.stop()
            self.stop_action.setDisabled(True)

        # Scenario -> GUI    widget updates
    def scenario_finished(self):
        if self.__tarfile:
            self.__tarfile.close()
            self.__tarfile = None
        self.start_action.setEnabled(True)
        self.stop_action.setDisabled(True)
        self.scenario_but.setEnabled(True)
        self.cam1_dock.live_mode_button.setEnabled(True)
        self.cam2_dock.live_mode_button.setEnabled(True)

    def change_camera_window(self, cam, coords):
        left, top, right, bottom = coords
        if cam == 0:
            self.cam1_dock.w_left.setValue(left)
            self.cam1_dock.w_right.setValue(right)
            self.cam1_dock.w_top.setValue(top)
            self.cam1_dock.w_bottom.setValue(bottom)
        else:
            self.cam2_dock.w_left.setValue(left)
            self.cam2_dock.w_right.setValue(right)
            self.cam2_dock.w_top.setValue(top)
            self.cam2_dock.w_bottom.setValue(bottom)

    def change_camera_binx(self, cam, binx):
        if cam == 0:
            self.cam1_dock.binx_sb.setValue(binx)
        else:
            self.cam2_dock.binx_sb.setValue(binx)

    def change_camera_biny(self, cam, biny):
        if cam == 0:
            self.cam1_dock.biny_sb.setValue(biny)
        else:
            self.cam2_dock.biny_sb.setValue(biny)

    def change_camera_integ_time(self, cam, integ_time):
        if cam == 0:
            self.cam1_dock.integ_time_sb.setValue(integ_time)
        else:
            self.cam2_dock.integ_time_sb.setValue(integ_time)

    def change_camera_aotf_freq(self, cam, aotf_freq):
        if cam == 0:
            self.cam1_dock.aotf_freq_sb.setValue(aotf_freq)
        else:
            self.cam2_dock.aotf_freq_sb.setValue(aotf_freq)

    def change_camera_frames_per_trigger(self, cam, frames_per_trigger):
        if cam == 0:
            self.cam1_dock.frames_per_trigger_sb.setValue(frames_per_trigger)
        else:
            self.cam2_dock.frames_per_trigger_sb.setValue(frames_per_trigger)

    def closeEvent(self, ev):
        self.camera_th.stop()
        print("Bye")


def run():
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.resize(MAIN_WIN_WIDTH, MAIN_WIN_HEIGHT)
    main_window.show()
    app.exec_()


if __name__ == "__main__":
    run()
