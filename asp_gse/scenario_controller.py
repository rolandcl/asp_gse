import os.path
import time

from PyQt5.QtCore import *

from asp_gse.config import *

class ScenarioThread(QThread):
    exec_line                 = pyqtSignal(str)
    change_camera_window      = pyqtSignal(int, list)
    change_binx               = pyqtSignal(int, int)
    change_biny               = pyqtSignal(int, int)
    change_integ_time         = pyqtSignal(int, int)
    change_aotf_freq          = pyqtSignal(int, int)
    change_frames_per_trigger = pyqtSignal(int, int)

    def __init__( self, path, queue):
        super().__init__()
        self.__scenario_path = path
        self.__queue = queue

    def iter_lines(self, fd):
        repeat = 1
        for line in fd:
            sline = line.strip()
            sline = sline.split("#")[0]
            if not sline:
                continue
            words = sline.split()
            try:
                command = words[0]
                args = [int(w) for w in words[1:]]
                if command == "repeat":
                    if len(args) != 1:
                        yield (None, "Error (Bad number of args)", line)
                        break
                    else:
                        repeat = args[0]
                else:
                    for _ in range(repeat):
                        yield (command, args, line)
                    repeat = 1
            except Exception as e:
                yield (None, e, line)

    def stop(self):
        self.__scenario_running = False

    def run( self ):
        self.__scenario_running = True
        basename = os.path.basename(self.__scenario_path)
        sweep_wait_time = SWEEP_WAIT_TIME / 1000
        with open(self.__scenario_path) as fd:
            for command, args, line in self.iter_lines(fd):
                self.exec_line.emit("Scenario %s> %s" % (basename, line))
                if not self.__scenario_running:
                    break
                if command is None:
                    print("Syntax Error (%s)" % args)
                    print("  at", line)
                elif command == "sweep_wait_time":
                    if len(args) == 1:
                        sweep_wait_time = args[0] / 1000
                    else:
                        print("Error (Bad number of args)\n  at", line)
                elif command == "window1":
                    if len(args) == 4:
                        self.change_camera_window.emit(0, args)
                    else:
                        print("Error (Bad number of args)\n  at", line)
                elif command == "window2":
                    if len(args) == 4:
                        self.change_camera_window.emit(1, args)
                    else:
                        print("Error (Bad number of args)\n  at", line)
                elif command == "binx":
                    if len(args) == 1:
                        self.change_binx.emit(0, args[0])
                        self.change_binx.emit(1, args[0])
                    else:
                        print("Error (Bad number of args)\n  at", line)
                elif command == "biny":
                    if len(args) == 1:
                        self.change_biny.emit(0, args[0])
                        self.change_biny.emit(1, args[0])
                    else:
                        print("Error (Bad number of args)\n  at", line)
                elif command == "integ_time1":
                    if len(args) == 1:
                        self.change_integ_time.emit(0, args[0])
                    else:
                        print("Error (Bad number of args)\n  at", line)
                elif command == "integ_time2":
                    if len(args) == 1:
                        self.change_integ_time.emit(1, args[0])
                    else:
                        print("Error (Bad number of args)\n  at", line)
                elif command == "frames_per_trigger":
                    if len(args) == 1:
                        self.change_frames_per_trigger.emit(0, args[0])
                        self.change_frames_per_trigger.emit(1, args[0])
                    else:
                        print("Error (Bad number of args)\n  at", line)
                elif command == "img":
                    if len(args) == 2:
                        self.change_aotf_freq.emit(0, args[0])
                        self.change_aotf_freq.emit(1, args[1])
                        self.__queue.put((args[0], args[1]))
                    elif len(args) == 6:
                        for freq1, freq2 in zip(range(args[0], args[1]+1, args[2]),
                                                range(args[3], args[4]+1, args[5])):
                            if not self.__scenario_running:
                                break
                            self.change_aotf_freq.emit(0, freq1)
                            self.change_aotf_freq.emit(1, freq2)
                            self.__queue.put((freq1, freq2))
                            time.sleep(sweep_wait_time)
                    else:
                        print("Error (Bad number of args)\n  at", line)
                elif command == "img+bkg":
                    if len(args) == 2:
                        self.change_aotf_freq.emit(0, args[0])
                        self.change_aotf_freq.emit(1, args[1])
                        self.__queue.put((args[0], args[1]))
                        self.__queue.put((0, 0))
                    elif len(args) == 6:
                        for freq1, freq2 in zip(range(args[0], args[1]+1, args[2]),
                                                range(args[3], args[4]+1, args[5])):
                            if not self.__scenario_running:
                                break
                            self.change_aotf_freq.emit(0, freq1)
                            self.change_aotf_freq.emit(1, freq2)
                            self.__queue.put((freq1, freq2))
                            time.sleep(sweep_wait_time)
                        self.__queue.put((0, 0))
                    else:
                        print("Error (Bad number of args)\n  at", line)
                elif command == "bkg":
                    if len(args) == 0:
                        self.__queue.put((0, 0))
                    else:
                        print("Error (Bad number of args)\n  at", line)
                elif command == "wait":
                    if len(args) == 1:
                        print("wait", args[0])
                        self.__queue.put((None, None)) ## Do nothing, but wait for camera thread
                        time.sleep(args[0]/1000)
                    else:
                        print("Error (Bad number of args)\n  at", line)
                else:
                    print("Invalid command :", command)
                    print("  at", line)
                #time.sleep(0.2) # FixMe
        self.exec_line.emit("Scenario %s Done" % basename)

