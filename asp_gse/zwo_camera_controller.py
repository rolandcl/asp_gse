# from contextlib import ExitStack
from datetime import datetime
import os
import os.path
import queue
import sys
import threading
import time

import numpy as np
from PyQt5.QtCore import *
from PIL import Image # FixMe : Only for tests

from asp_gse.config import *
import asp_gse.zwoasi as zwoasi
from asp_gse.asp_frame import AspFrame
from asp_gse.cmods6_controller import CmodS6Controller
from asp_gse.inclinometer import Inclinometer

ZWO_CAMERA1_SN = b'1b07c90a16'
ZWO_CAMERA2_SN = b'1b2dc60a16'

# On Windows, add DLLs to $PATH
if os.name == "nt":
    abs_path_to_file_dir = os.path.dirname(os.path.abspath(__file__))
    abs_path_to_dlls = os.path.join(abs_path_to_file_dir, "dlls")
    os.environ['PATH'] = abs_path_to_dlls + os.pathsep + os.environ['PATH']

class CameraThread(QObject, threading.Thread):
    new_img_sig1 = pyqtSignal(object)
    new_img_sig2 = pyqtSignal(object)

    def __init__(self, queue):
        super().__init__()
        self.__running = True
        self.__live_mode = False
        self.__reinit_pending = False
        self.__w_left1   = DEFAULT_W_LEFT
        self.__w_top1    = DEFAULT_W_TOP
        self.__w_right1  = DEFAULT_W_RIGHT
        self.__w_bottom1 = DEFAULT_W_BOTTOM
        self.__w_left2   = DEFAULT_W_LEFT
        self.__w_top2    = DEFAULT_W_TOP
        self.__w_right2  = DEFAULT_W_RIGHT
        self.__w_bottom2 = DEFAULT_W_BOTTOM
        self.__binx = 1
        self.__biny = 1
        self.__cam_binning = 1
        self.__integ_time1 = DEFAULT_INTEG_TIME
        self.__integ_time2 = DEFAULT_INTEG_TIME
        self.__aotf_freq1 = DEFAULT_AOTF_FREQ
        self.__aotf_freq2 = DEFAULT_AOTF_FREQ
        self.__frames_per_trigger = 1
        self.__queue = queue

    def stop(self):
        self.__running = False

    def trigger(self):
        self.__queue.put((int(self.__aotf_freq1), int(self.__aotf_freq2)))

    def live_mode(self, flag):
        self.__live_mode = flag

    def set_w_left1(self, w_left):
        self.__w_left1 = w_left
        self.__reinit_pending = True

    def set_w_right1(self, w_right):
        self.__w_right1 = w_right
        self.__reinit_pending = True

    def set_w_top1(self, w_top):
        self.__w_top1 = w_top
        self.__reinit_pending = True

    def set_w_bottom1(self, w_bottom):
        self.__w_bottom1 = w_bottom
        self.__reinit_pending = True

    def set_w_left2(self, w_left):
        self.__w_left2 = w_left
        self.__reinit_pending = True

    def set_w_right2(self, w_right):
        self.__w_right2 = w_right
        self.__reinit_pending = True

    def set_w_top2(self, w_top):
        self.__w_top2 = w_top
        self.__reinit_pending = True

    def set_w_bottom2(self, w_bottom):
        self.__w_bottom2 = w_bottom
        self.__reinit_pending = True

    def set_cam_binning(self, cam_binning):
        self.__cam_binning = cam_binning

    def set_binx(self, binx):
        self.__binx = binx
        #self.__reinit_pending = True

    def set_biny(self, biny):
        self.__biny = biny
        #self.__reinit_pending = True

    def set_frames_per_trigger(self, n):
        self.__frames_per_trigger = n
        self.__reinit_pending = True

    def set_integ_time1(self, integ_time):
        self.__integ_time1 = integ_time

    def set_integ_time2(self, integ_time):
        self.__integ_time2 = integ_time

    def set_aotf_freq1(self, aotf_freq):
        self.__aotf_freq1 = aotf_freq

    def set_aotf_freq2(self, aotf_freq):
        self.__aotf_freq2 = aotf_freq


    def run(self):
        # with CmodS6Controller() as cmods6, Inclinometer() as incli:
            cam1 = None
            cam2 = None
            n = zwoasi.get_num_cameras()

            for i in range(n):
                cam = zwoasi.Camera(i)
                print(cam.get_sn())
                if cam.get_sn() == ZWO_CAMERA1_SN:
                    cam1 = cam
                elif cam.get_sn() == ZWO_CAMERA2_SN:
                    cam2 = cam

            if cam1 is None:
                print("Camera %s not found." % ZWO_CAMERA1_SN)
                sys.exit(0)
            if cam2 is None:
                print("Camera %s not found." % ZWO_CAMERA2_SN)
                sys.exit(0)



            # controls = cam1.get_controls()
            # for cn in sorted(controls.keys()):
            #     print('    %s:' % cn)
            #     for k in sorted(controls[cn].keys()):
            #         print('        %s: %s' % (k, repr(controls[cn][k])))

            cam1.set_image_type(zwoasi.ASI_IMG_RAW16)
            cam2.set_image_type(zwoasi.ASI_IMG_RAW16)

            frame_cnt = 0
            prev_roi1 = None
            prev_roi2 = None
            while self.__running:
                try:
                    if self.__live_mode:
                        freq1 = self.__aotf_freq1
                        freq2 = self.__aotf_freq2
                        time.sleep(LIVE_MODE_SLEEP_TIME)
                    else:
                        freq1, freq2 = self.__queue.get(timeout=0.1)
                except queue.Empty:
                    cam1.stop_video_capture()
                    cam2.stop_video_capture()
                    continue
                else:
                    integ_time1 = self.__integ_time1
                    integ_time2 = self.__integ_time2
                    binx = self.__binx
                    biny = self.__biny
                    cam_binning = self.__cam_binning
                    frames_per_trigger = self.__frames_per_trigger


                    cam1.set_control_value(zwoasi.ASI_EXPOSURE, integ_time1)
                    cam1.set_control_value(zwoasi.ASI_OFFSET, 4)
                    cam2.set_control_value(zwoasi.ASI_EXPOSURE, integ_time2)
                    cam2.set_control_value(zwoasi.ASI_OFFSET, 4)
                    
                    l1 = self.__w_left1 // cam_binning
                    t1 = self.__w_top1 // cam_binning
                    w1 = self.__w_right1 - self.__w_left1
                    w1 = w1 // 8 * 8 // cam_binning
                    h1 = self.__w_bottom1 - self.__w_top1
                    h1 = h1 // 8 * 8 // cam_binning

                    l2 = self.__w_left2 // cam_binning
                    t2 = self.__w_top2 // cam_binning
                    w2 = self.__w_right2 - self.__w_left2
                    w2 = w2 // 8 * 8 // cam_binning
                    h2 = self.__w_bottom2 - self.__w_top2
                    h2 = h2 // 8 * 8 // cam_binning

                    if w1 < 32 or h1 < 32:
                        continue
                    if w2 < 32 or h2 < 32:
                        continue

                    roi1 = (l1, t1, w1, h1, cam_binning)
                    roi2 = (l2, t2, w2, h2, cam_binning)
                    if roi1 != prev_roi1:
                        time.sleep(0.05)
                        cam1.stop_video_capture()
                        time.sleep(0.05)
                        cam1.set_roi(*roi1)
                        prev_roi1 = roi1
                        time.sleep(0.05)
                    if roi2 != prev_roi2:
                        time.sleep(0.05)
                        cam2.stop_video_capture()
                        time.sleep(0.05)
                        cam2.set_roi(*roi2)
                        prev_roi2 = roi2
                        time.sleep(0.05)

                    if self.__live_mode:
                        n_frames = 1
                    else:
                        n_frames = frames_per_trigger
                    cam1.start_video_capture()
                    cam2.start_video_capture()
                    for _ in range(n_frames):
                        img1 = cam1.capture_video_frame()
                        img2 = cam2.capture_video_frame()
                        dtime = datetime.now()
                        asp_fr = AspFrame(img1, l1, t1, binx, biny)
                        asp_fr.dtime = dtime
                        asp_fr.aotf_freq = freq1
                        asp_fr.frame_cnt = frame_cnt
                        asp_fr.integ_time = integ_time1
                        #asp_fr.temp0 = temp0
                        #asp_fr.temp1 = temp1
                        #asp_fr.x_incl = x_incl
                        #asp_fr.y_incl = y_incl
                        #asp_fr.camera = CAMERA_1
                        self.new_img_sig1.emit(asp_fr)

                        asp_fr = AspFrame(img2, l2, t2, binx, biny)
                        asp_fr.dtime = dtime
                        asp_fr.aotf_freq = freq2
                        asp_fr.frame_cnt = frame_cnt
                        asp_fr.integ_time = integ_time2
                        #asp_fr.temp0 = temp0
                        #asp_fr.temp1 = temp1
                        #asp_fr.x_incl = x_incl
                        #asp_fr.y_incl = y_incl
                        #asp_fr.camera = CAMERA_1
                        self.new_img_sig2.emit(asp_fr)

                        
                        frame_cnt += 1
                        #time.sleep(0.01)


    def run_old(self):
        with TLCameraSDK() as sdk, CmodS6Controller() as cmods6, Inclinometer() as incli:
            available_cameras = sdk.discover_available_cameras()
            if CAMERA_1 not in available_cameras:
                print("Camera '%s' not found" % CAMERA_1)
            elif CAMERA_2 not in available_cameras:
                print("Camera '%s' not found" % CAMERA_2)
            else:
                curr_freq1 = None
                curr_freq2 = None
                last_trigger_time = 0
                while self.__running:
                    self.__reinit_pending = False
                    with sdk.open_camera(CAMERA_1) as cam1, sdk.open_camera(CAMERA_2) as cam2:
                        curr_integ_time = None
                        cam1.frames_per_trigger_zero_for_unlimited = self.__frames_per_trigger
                        cam2.frames_per_trigger_zero_for_unlimited = self.__frames_per_trigger
                        cam1.image_poll_timeout_ms = 10  # 0.01 second polling timeout
                        cam2.image_poll_timeout_ms = 10  # 0.01 second polling timeout
                        print("Reinit cameras")
                        #print(self.camera_info(cam1))
                        cam1.roi = (self.__w_left1, self.__w_top1,
                                      self.__w_right1, self.__w_bottom1)
                        cam2.roi = (self.__w_left2, self.__w_top2,
                                      self.__w_right2, self.__w_bottom2)
                        cam1.binx = 1
                        cam1.biny = 1
                        cam2.binx = 1
                        cam2.biny = 1
                        cam1.exposure_time_us = self.__integ_time1
                        cam2.exposure_time_us = self.__integ_time2
                        frames_per_trigger = cam1.frames_per_trigger_zero_for_unlimited or 1
                        cam1.arm(frames_per_trigger)
                        cam2.arm(frames_per_trigger)
                        prev_integ_time1 = None
                        prev_integ_time2 = None
                        while self.__running and not self.__reinit_pending:
                            try:
                                if self.__live_mode:
                                    freq1 = self.__aotf_freq1
                                    freq2 = self.__aotf_freq2
                                    time.sleep(LIVE_MODE_SLEEP_TIME)
                                else:
                                    freq1, freq2 = self.__queue.get(timeout=0.1)
                            except queue.Empty:
                                continue
                            else:
                                if freq1 is None:
                                    continue

                                # If needed, Update AOTF frequency
                                sleep_flag = False
                                if freq1 != curr_freq1:
                                    cmods6.set_aotf_freq1(freq1)
                                    sleep_flag = True
                                if freq2 != curr_freq2:
                                    cmods6.set_aotf_freq2(freq2)
                                    sleep_flag = True
                                if sleep_flag:
                                    time.sleep(0.02)

                                # From Thorlabs API Reference:
                                # After issuing a soft trigger, wait at least 300ms 
                                # before setting exposure
                                integ_t1_changed = prev_integ_time1 != self.__integ_time1
                                integ_t2_changed = prev_integ_time2 != self.__integ_time2
                                if integ_t1_changed or integ_t2_changed:
                                    now = time.time()
                                    sleep_time = 0.3 - now + last_trigger_time
                                    #print("integ time changed, sleep", sleep_time)
                                    if sleep_time > 0:
                                        time.sleep(sleep_time)
                                    cam1.exposure_time_us = self.__integ_time1
                                    cam2.exposure_time_us = self.__integ_time2
                                    prev_integ_time1 = self.__integ_time1
                                    prev_integ_time2 = self.__integ_time2

                                binx = self.__binx
                                biny = self.__biny

                                cam1.issue_software_trigger()
                                cam2.issue_software_trigger()
                                last_trigger_time = time.time()
                                temp0 = cmods6.get_temperature(0)
                                temp1 = cmods6.get_temperature(1)
                                x_incl, y_incl = incli.ask()
                                frame1 = None
                                frame2 = None
                                for _ in range(frames_per_trigger):
                                    exp_t1 = time.time()
                                    exp_t2 = exp_t1 + cam2.exposure_time_us / 1e6 + 0.5
                                    exp_t1 = exp_t1 + cam1.exposure_time_us / 1e6 + 0.5
                                    while frame1 is None:
                                        frame1 = cam1.get_pending_frame_or_null()
                                        if time.time() > exp_t1:
                                            break
                                        time.sleep(0.05)
                                    if frame1 is None:
                                        self.__live_mode = False
                                        self.new_img_sig1.emit(None)
                                        break
                                    while frame2 is None:
                                        frame2 = cam2.get_pending_frame_or_null()
                                        if time.time() > exp_t2:
                                            break
                                        time.sleep(0.05)
                                    if frame2 is None:
                                        self.__live_mode = False
                                        self.new_img_sig2.emit(None)
                                        break
                                    dtime = datetime.now()

                                    frame_count1 = frame1.frame_count
                                    fr1 = np.copy(frame1.image_buffer)
                                    frame_count2 = frame2.frame_count
                                    fr2 = np.copy(frame2.image_buffer)
                                    print("Cam1 frame #{} received!".format(frame_count1))
                                    frame1 = cam1.get_pending_frame_or_null()
                                    print("Cam2 frame #{} received!".format(frame_count2))
                                    frame2 = cam2.get_pending_frame_or_null()

                                    asp_fr = AspFrame(fr1, cam1.roi[0], cam1.roi[1],
                                                          binx, biny)
                                    asp_fr.dtime = dtime
                                    asp_fr.aotf_freq = freq1
                                    asp_fr.frame_cnt = frame_count1
                                    asp_fr.integ_time = cam1.exposure_time_us
                                    asp_fr.temp0 = temp0
                                    asp_fr.temp1 = temp1
                                    asp_fr.x_incl = x_incl
                                    asp_fr.y_incl = y_incl
                                    asp_fr.camera = CAMERA_1
                                    self.new_img_sig1.emit(asp_fr)

                                    asp_fr = AspFrame(fr2, cam2.roi[0], cam2.roi[1],
                                                          binx, biny)
                                    asp_fr.dtime = dtime
                                    asp_fr.aotf_freq = freq2
                                    asp_fr.frame_cnt = frame_count2
                                    asp_fr.integ_time = cam2.exposure_time_us
                                    asp_fr.temp0 = temp0
                                    asp_fr.temp1 = temp1
                                    asp_fr.x_incl = x_incl
                                    asp_fr.y_incl = y_incl
                                    asp_fr.camera = CAMERA_2
                                    self.new_img_sig2.emit(asp_fr)

        #cmods6.close()

    def camera_info(self, camera):
        buf = ""
        buf += "model = %s\n" % camera.model
        buf += "usb_port_type = %s\n" % camera.usb_port_type
        buf += "get_measured_frame_rate_fps = %s\n" % camera.get_measured_frame_rate_fps()
        buf += "sensor_readout_time_ns = %s\n" % str(camera.sensor_readout_time_ns)
        buf += "exposure_time_range_us = %s\n" % str(camera.exposure_time_range_us)
        buf += "is_eep_supported = %s\n" % camera.is_eep_supported
        buf += "is_hot_pixel_correction_enabled = %s\n" % camera.is_hot_pixel_correction_enabled
        buf += "binx_range = %s\n" % str(camera.binx_range)
        buf += "biny_range = %s\n" % str(camera.biny_range)
        buf += "roi_range = %s\n" % str(camera.roi_range)
        buf += "sensor_width_pixels = %s\n" % camera.sensor_width_pixels
        buf += "sensor_height_pixels = %s\n" % camera.sensor_height_pixels
        buf += "data_rate = %s\n" % camera.data_rate
        buf += "bit_depth = %s\n" % camera.bit_depth
        buf += "gain_range = %s\n" % str(camera.gain_range)
        buf += "frames_per_trigger_range = %s\n" % str(camera.frames_per_trigger_range)
        return buf

if __name__ == "__main__":
    q = queue.Queue(1)
    cam_th = CameraThread(q)
    cam_th.set_integ_time1(2000)
    cam_th.set_integ_time2(2000)
    cam_th.start()
    time.sleep(5)
    #cam_th.set_integ_time1(15000)
    #time.sleep(4)
    cam_th.stop()
    cam_th.join()
