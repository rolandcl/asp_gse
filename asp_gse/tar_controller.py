from datetime import datetime
import io
import os.path
import tarfile

from astropy.io import fits
import numpy as np
from PIL import Image as PIL_Image
from PIL.PngImagePlugin import PngInfo
from PyQt5.QtCore import *

from asp_gse.config import *

class TarFile(QObject):
    def __init__(self, scenario_path):
        super().__init__()
        str_date = datetime.now().strftime("%Y%m%d_%H%M%S")
        scenario_bname = os.path.basename(scenario_path)
        tarname = "%s_%s.tar" % (str_date, scenario_bname)
        tarpath = os.path.join(DEST_PATH, tarname)
        self.__tarfile = tarfile.open(tarpath, "w")
        self.__tarfile.add(scenario_path, scenario_bname)
        self.n_of_frames = 0

    def save_asp_frame(self, asp_frame):
        if asp_frame is None:
            return
        str_time = asp_frame.dtime.strftime("%Y%m%d_%H%M%S_%f")[:-3]
        # ToDo: add line number in scenario file to filename
        # ToDo: add integ_time to filename ??
        filename = "%s_Cam%s_%d.fits" % (str_time, asp_frame.camera, asp_frame.aotf_freq)
        bytes_io = io.BytesIO()

        if asp_frame.binx > 1 and asp_frame.biny > 1:
            ## Apply binning
            fr =  asp_frame.frame
            binx = asp_frame.binx
            biny = asp_frame.biny
            height, width = fr.shape
            h = height // biny * biny
            w = width // binx * binx

            a = fr[:h,:w].reshape((h//biny,biny,w//binx,binx)).sum(-1).sum(1)
            if h != height:
                b = fr[h:height,:w].reshape(1,height-h, w//binx, binx).sum(-1).sum(1)
                a = np.concatenate((a,b), axis=0)
            if w != width:
                c = fr[:h,w:width].reshape(h//biny,biny, 1, width-w).sum(-1).sum(1)
                if h != height:
                    d = fr[h:height, w:width].reshape(1,height-h, 1, width-w).sum(-1).sum(1)
                    c = np.concatenate((c,d), axis=0)
                a = np.concatenate((a,c), axis=1)
            a = a.astype(np.uint32)
            bitpix = 32
        else:
            a = asp_frame.frame
            bitpix = 16

        header = fits.Header()
        header["w_left"] = asp_frame.x
        header["w_top"] = asp_frame.y
        header["binx"] = asp_frame.binx
        header["biny"] = asp_frame.biny
        header["temp0"] = asp_frame.temp0
        header["temp1"] = asp_frame.temp1
        header["x_incl"] = asp_frame.x_incl
        header["y_incl"] = asp_frame.y_incl
        header["fr_cntr"] = asp_frame.frame_cnt
        header["integ_t"] = asp_frame.integ_time
        header["ser_num"] = asp_frame.camera
        header["bitpix"] = bitpix
        header["naxis"] = 2
        hdu_im = fits.CompImageHDU(a, header)
        hdu_im.writeto(bytes_io)

        buf = bytes_io.getvalue()
        tar_info = tarfile.TarInfo(filename)
        tar_info.size = len(buf)
        self.__tarfile.addfile(tar_info, io.BytesIO(buf))
        self.n_of_frames += 1

    def save_asp_frame_as_png(self, asp_frame):
        str_time = asp_frame.dtime.strftime("%Y%m%d_%H%M%S_%f")[:-3]
        # ToDo: add line number in scenario file to filename
        # ToDo: add integ_time to filename ??
        filename = "%s_Cam%s_%d.png" % (str_time, asp_frame.camera, asp_frame.aotf_freq)
        bytes_io = io.BytesIO()
        img = PIL_Image.fromarray(asp_frame.frame)
        metadata = PngInfo()
        metadata.add_text("window_left", str(asp_frame.x))
        metadata.add_text("window_top", str(asp_frame.y))
        metadata.add_text("window_binx", str(asp_frame.binx))
        metadata.add_text("window_biny", str(asp_frame.biny))
        metadata.add_text("temp0", str(asp_frame.temp0))
        metadata.add_text("temp1", str(asp_frame.temp1))
        metadata.add_text("x_incl", str(asp_frame.x_incl))
        metadata.add_text("y_incl", str(asp_frame.y_incl))
        metadata.add_text("frame_cnt", str(asp_frame.frame_cnt))
        metadata.add_text("integ_time", str(asp_frame.integ_time))
        img.save(bytes_io, "PNG", pnginfo=metadata)
        buf = bytes_io.getvalue()
        tar_info = tarfile.TarInfo(filename)
        tar_info.size = len(buf)
        self.__tarfile.addfile(tar_info, io.BytesIO(buf))

    def close(self):
        self.__tarfile.close()

