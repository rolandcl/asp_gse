import time

import serial
import serial.tools.list_ports

CMD = b"\x68\x04\x00\x04\x08"

class Inclinometer(serial.Serial):
    def __init__(self):
        ports = list(serial.tools.list_ports.grep("USB2Serial|BBUSER232"))
        if not ports:
            raise serial.SerialException("Inclinometer USB2Serial adapter not found")
        super().__init__(ports[0].device, baudrate=9600, timeout=0.1)

    def ask(self):
        self.write(CMD)
        buf = self.read(42)
        #print(" ".join("%02x" % x for x in buf))
        if not buf or buf[0] != 0x68:
            return None, None
        x = (buf[4] << 16) + (buf[5] << 8) + buf[6]
        if x >= 0x100000:
            x = 0x100000 - x
        y = (buf[7] << 16) + (buf[8] << 8) + buf[9]
        if y >= 0x100000:
            y = 0x100000 - y
        return x,y

if __name__ == "__main__":
    with Inclinometer() as i:
        for _ in range(5):
            x,y = i.ask()
            print(x,y)
            time.sleep(1)
